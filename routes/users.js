﻿'use strict';
var express = require('express');
var moveFile = require('../utils/fs/move');
var fileService = require('../service/fileService');
var User = require('../schemas/user');
var Recipe = require('../schemas/recipe');
var encrypt = require('../auth/encrypt');
var verifyToken = require('../auth/verifyToken');
var notificationService = require('../service/notificationService');
var router = express.Router();

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

/* GET users listing. */
router.get('/me', verifyToken, function (req, res) {
    try {
        req.user
        .populate('friends', 'name email image')
        .populate('pendingRequests', 'name email image')
        .populate({
            path: 'favoriteRecipes',
            populate: [
                {
                    path: 'ingredients',
                    populate: {
                        path: 'ingredient',
                        populate: {
                            path: 'type'
                        }
                    }
                },
                {
                    path: 'steps'
                }
            ]
        }, function (err, user) {
            if (err) {
                return res.send({ status: 500, message: "Something went wrong." });
            } else {
                res.send({ status: 200, user: user });
            }
        });
    } catch (err) {
        console.log(err);
    }
});

router.post('/me', verifyToken, function (req, res) {

    var data = {};
    if (req.body.fcmRefreshToken) {
        data.fcmRefreshToken = req.body.fcmRefreshToken
    }

    User.findOneAndUpdate(
        { _id: req.user.id },
        data,
        function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "User could not be updated."
                });
            } else {
                res.send({
                    status: 200,
                    message: "User updated."
                });
            }
        }
    );
});

// upload a profile picture
router.post('/me/image', verifyToken, multipartMiddleware, function (req, res) {
    try {
        for (var i in req.files) {
            var file = req.files[i],
                newPath = fileService.getMediaFilePath(file);

            moveFile(file.path, process.cwd() + '/public' + newPath, function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        message: "Cannot upload profile image"
                    });
                } else {
                    User.findOneAndUpdate(
                        { _id: req.user._id },
                        { image: newPath },
                        function (err, a) {
                            if (err) {
                                res.send({
                                    status: 500,
                                    message: "Cannot upload profile image"
                                });
                            } else {
                                res.send({
                                    status: 200,
                                    message: "Profile image was uploaded",
                                    path: newPath
                                });
                            }
                        }
                    );
                }
            });
        }
    } catch (e) {
        console.log(e);
    }
});

/* Create a new user */
router.post('/', function (req, res) {
    User.findOne({ "email": req.body.email }).lean().exec(function (err, user) {
        if (err) {
            return res.send({status: 500, message: "Something went wrong." });
        }
        if (!user) {
            var user = new User(req.body);

            if (user.provider === "" || user.provider === "chefs_app") {
                var encryptionData = encrypt(user.password);

                user.salt = encryptionData.salt;
                user.password = encryptionData.passwordHash;
            }

            user.save(function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        message: "Cannot create user"
                    });
                } else {
                    res.send({
                        status: 200,
                        message: "User created"
                    });
                }
            });
        } else {
            return res.send({ status: 400, message: "User with this email already exists!" });
        }
    });
});

/* Find users */
router.get('/', verifyToken, function (req, res) {
    var term = req.query.term.replace(/[^\w\s.@_-]/gi, '');
    var regexCondition = {
        $regex: term,
        $options: 'i'
    };

    try {
        User.find(
            {
                $and: [
                    {
                        $or: [
                            {'name': regexCondition},
                            {'email': regexCondition},
                        ]
                    },
                    {
                        '_id': {
                            $nin: req.user.friends
                        }
                    },
                    {
                        '_id': {
                            $nin: req.user.pendingRequests
                        }
                    }
                ]
            },
            'name email',
            function (err, users) {
                if (err) {
                    console.log(err);
                    return res.send({
                        status: 500,
                        message: "Users not found"
                    });
                } else {
                    res.send({ status: 200, users: users });
                }
            }
        );
    } catch (err) {
        console.log(err);
    }
});

router.post('/me/reject-friendship-request/:friendId', verifyToken, function (req, res) {
    User.findOneAndUpdate(
        { _id: req.user._id },
        {
            $pull: { pendingRequests: req.params.friendId }
        },
        function (err, friend) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Friendship request could not be rejected."
                });
            } else {
                res.send({
                    status: 200,
                    message: "Friendship request rejected."
                });
            }
        }
    );
});
router.post('/me/accept-friendship-request/:friendId', verifyToken, function (req, res) {
    User.findOneAndUpdate(
        { _id: req.params.friendId },
        {
            $push: { friends: req.user._id }
        },
        function (err, friend) {
            if (err) {
                return res.send({
                    status: 500,
                    message: "User was not found"
                });
            } else {
                User.findOneAndUpdate(
                    { _id: req.user.id },
                    {
                        $push: { friends: friend.id },
                        $pull: { pendingRequests: friend.id }
                    },
                    function (err) {
                        if (err) {
                            res.send({
                                status: 500,
                                message: "Friendship request could not be accepted."
                            });
                        } else {
                            notificationService.sendAcceptFriendshipNotification(req.user, friend);
                            res.send({
                                status: 200,
                                message: "Friendship request accepted."
                            });
                        }
                    }
                );
            }
        }
    );
});
router.post('/me/send-friendship-request/:friendId', verifyToken, function (req, res) {
    User.findOneAndUpdate(
        { _id: req.params.friendId },
        { $push: { pendingRequests: req.user }},
        function (err, friend) {
            if (err) {
                return res.send({
                    status: 500,
                    message: "Friendship request could not be send."
                });
            }
            notificationService.sendFriendshipRequestNotification(req.user, friend);
            res.send({
                status: 200,
                message: "Friendship request send."
            });
        }
    );
});
router.delete('/me/friendship/:friendId', verifyToken, function (req, res) {
    User.findOneAndUpdate(
        { _id: req.user.id },
        {
            $pull: { friends: req.params.friendId },
        },
        function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Friend could not be removed"
                });
            } else {
                User.findOneAndUpdate(
                    { _id: req.params.friendId },
                    {
                        $pull: { friends: req.user.id },
                    },
                    function (err) {
                        if (err) {
                            res.send({
                                status: 500,
                                message: "Friend could not be removed"
                            });
                        } else {
                            res.send({
                                status: 200,
                                message: "Friend has been removed"
                            });
                        }
                    }
                );
            }
        }
    );
});

router.post('/me/favorite-recipes/:recipeId', verifyToken, function (req, res) {
    Recipe.findById(req.params.recipeId, function (err, recipe) {
        if (err) {
            return res.send({
                status: 500,
                message: "Recipe was not found"
            });
        }
        User.findOneAndUpdate(
            { _id: req.user.id },
            { $push: { favoriteRecipes: recipe }},
            function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        message: "Recipe could not be marked as favorite"
                    });
                } else {
                    res.send({
                        status: 200,
                        message: "Recipe marked as favorite"
                    });
                }
            }
        );
    });
});

router.delete('/me/favorite-recipes/:recipeId', verifyToken, function (req, res) {
    User.findOneAndUpdate(
        { _id: req.user.id },
        { $pull: { favoriteRecipes: req.params.recipeId }},
        function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Recipe could not be marked as favorite"
                });
            } else {
                res.send({
                    status: 200,
                    message: "Recipe marked as favorite"
                });
            }
        }
    );
});



module.exports = router;
