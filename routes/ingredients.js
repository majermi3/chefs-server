'use strict';
var express = require('express');
var Ingredient = require('../schemas/ingredient');
var IngredientType = require('../schemas/ingredientType');
var cache = require('../utils/cache.js');
var verifyToken = require('../auth/verifyToken');
var router = express.Router();
var nodemailer = require('nodemailer');
var fs = require('fs');

/* GET ingredients */
router.get('/', cache(20), verifyToken, function (req, res) {
    Ingredient.find({
        $or: [
            { verified: true },
            { user: req.user.id }
        ]
    })
    .populate('type').exec(function (err, ingredients) {
        if (err) {
            res.send({ status: 500, message: "Something went wrong" });
        }
        res.send({ status: 200, ingredients: ingredients });
    });
});

router.get('/:ingredient/verification', function (req, res) {

    IngredientType.find(function (err, types) {
        if (err) {
            res.send({ status: 500, message: "Something went wrong" });
        }
        Ingredient.findById(req.params.ingredient, function (err, ingredient) {
            if (err) {
                return res.send({ status: 500, message: "Something went wrong" });
            }
            if (!ingredient) {
                return res.send({ status: 500, message: "Ingredient not found" });
            }
            res.render('../private/template/ingredients/verification', { ingredient: ingredient, types: types })
        });
    });
});

/* GET ingredients */
// TODO add secure token
// TODO create a UI to be able to set liquid and type properties
router.post('/:ingredient/verify', function (req, res) {
    Ingredient.findById(req.params.ingredient, function (err, ingredient) {
        if (err) {
            return res.send({ status: 500, message: "Something went wrong" });
        }
        if (!ingredient) {
            return res.send({ status: 500, message: "Ingredient not found" });
        }

        ingredient.verified = true;
        ingredient.type = req.body.type;
        ingredient.liquid = req.body.liquid;

        ingredient.save(function (err) {
            if (err) {
                res.send({ status: 500, message: "Something went wrong" });
            } else {
                res.send({ status: 200, message: "Ingredient was verified."});
            }
        });
    });
});


/* Create a new ingredient */
router.post('/', verifyToken, function (req, res, next) {

    var data = req.body;

    var ingredient = new Ingredient(data);
    ingredient.user = req.user;

    ingredient.save(function (err, createdIngredient) {
        if (err) {
            res.send({
                status: 500,
                message: "Cannot create ingredient"
            });
        } else {
            // send email to verify the ingredient

            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: global.config.email
            });
            var mailOptions = {
                from: 'no-reply@chefsapp.com',
                to: global.config.email.user,
                subject: 'New ingredient was created',
                html: 'Hello Chef,<br><br> please confirm the following ingredient:<br> title' + data.title
                    + '<br> <br><br><a href="' + global.config.baseUrl + '/ingredients/' + createdIngredient.id + '/verification">Confirm</a>'
            };
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });

            res.send({
                status: 200,
                message: "Ingredient created",
                id: createdIngredient.id
            });
        }
    });

});

module.exports = router;
