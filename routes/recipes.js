'use strict';
var express = require('express');
var moveFile = require('../utils/fs/move');
var fileService = require('../service/fileService');
var verifyToken = require('../auth/verifyToken');
var Recipe = require('../schemas/recipe');
var RecipeIngredient = require('../schemas/recipeIngredient');
var RecipeStep = require('../schemas/recipeStep');
var constants = require('../constants');
var ACCESS_LEVEL = constants.ACCESS_LEVEL;
var router = express.Router();
var mongoose = require('mongoose');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

mongoose.Promise = global.Promise;

router.get('/', verifyToken, function (req, res) {
    Recipe
    .find({ accessLevel: ACCESS_LEVEL.PUBLIC, deleted: false })
    .populate({
        path: 'ingredients',
        populate: {
            path: 'ingredient',
            populate: {
                path: 'type'
            }
        }
    })
    .populate('steps')
    .exec(function (err, recipes) {
        if (err) {
            res.send({ status: 500, message: "Something went wrong" });
        }
        res.send({ status: 200, recipes: recipes });
    });
});

/* GET current user recipes */
router.get('/my', verifyToken, function (req, res) {
    Recipe
    .find({ user: req.user, deleted: false })
    .populate({
        path: 'ingredients',
        populate: {
            path: 'ingredient',
            populate: {
                path: 'type'
            }
        }
    })
    .populate('steps')
    .exec(function (err, recipes) {
        if (err) {
            res.send({ status: 500, message: "Something went wrong" });
        }
        res.send({ status: 200, recipes: recipes });
    });
});

/* Create a new recipe */
router.post('/', verifyToken, function (req, res) {

    var data = req.body;

    var recipe = new Recipe({
        title: data.title,
        description: data.description,
        accessLevel: data.accessLevel,
        user: req.user
    });

    var promises = [];
    for (var i = 0, maxi = data.ingredients.length; i < maxi; i++) {
        var recipeIngredient = new RecipeIngredient(data.ingredients[i]);
        promises.push(recipeIngredient.save());
    }

    Promise.all(promises).then(function (ingredients) {
        recipe.ingredients = ingredients;
        recipe.save(function (err, createdRecipe) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot create recipe"
                });
            } else {
                res.send({
                    status: 200,
                    message: "Recipe created",
                    id: createdRecipe.id
                });
            }
        });
    }).catch(function(err) {
        res.send({
            status: 500,
            message: "Cannot create recipte ingredients"
        });
    });
});

/* Update a recipe */
router.put('/', verifyToken, function (req, res) {

    var data = req.body;

    Recipe.findById(data._id).populate('user').exec().then(function (recipe) {
        if (recipe && req.user.id === recipe.user.id) {
            recipe.title = data.title;
            recipe.description = data.description;
            recipe.accessLevel = data.accessLevel;

            recipe.save(function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        message: "Cannot create recipe"
                    });
                } else {
                    res.send({
                        status: 200,
                        message: "Recipe updated",
                        id: recipe.id
                    });
                }
            });
        } else {
            res.send({
                status: 500,
                message: "Cannot update recipe"
            });
        }
    });
});

router.delete('/:recipe', verifyToken, function (req, res) {
    Recipe.findOneAndUpdate(
        { _id: req.params.recipe },
        { $set: { deleted: true } },
        function(err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "The recipe could not be deleted"
                });
            } else {
                res.send({
                    status: 200,
                    message: "The recipe was deleted"
                });
            }
        }
    );
});

router.post('/:recipe/ingredients/', verifyToken, function (req, res) {

        var data = req.body;

        Recipe.findById(req.params.recipe).populate('user').exec().then(function (recipe) {
            if (recipe && req.user.id === recipe.user.id) {
                var recipeIngredient = new RecipeIngredient(data);

                recipeIngredient.save(function(err, createdRecipeIngredient) {
                    if (createdRecipeIngredient) {
                        Recipe.update(
                            {_id: recipe.id},
                            {$push: {ingredients: createdRecipeIngredient}},
                            function (err) {
                                if (err) {
                                    res.send({
                                        status: 500,
                                        message: "Cannot add ingredient to recipe"
                                    });
                                } else {
                                    res.send({
                                        status: 200,
                                        message: "Recipe ingredient added",
                                        id: createdRecipeIngredient.id
                                    });
                                }
                            }
                        );
                    } else {
                        res.send({
                            status: 500,
                            message: "Cannot create recipe ingredient"
                        });
                    }
                })
            } else {
                res.send({
                    status: 500,
                    message: "Cannot create recipe ingredient"
                });
            }
        });

});


router.delete('/:recipe/ingredients/:recipeIngredient', verifyToken, function (req, res) {
    Recipe.findOneAndUpdate(
        { _id: req.params.recipe },
        { $pull: { ingredients: req.params.recipeIngredient } },
        function (err, a) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot remove recipe ingredient from recipe"
                });
            } else {
                RecipeIngredient.remove({ _id: req.params.recipeIngredient }, function(err) {
                    if (err) {
                        res.send({
                            status: 500,
                            message: "Cannot remove recipe ingredient"
                        });
                    } else {
                        res.send({
                            status: 200,
                            message: "Recipe ingredient removed"
                        });
                    }
                });
            }
        }
    );
});

router.put('/:recipe/ingredients/:recipeIngredient', verifyToken, function (req, res) {
    RecipeIngredient
    .findByIdAndUpdate(req.params.recipeIngredient, req.body)
    .then(function (updatedRecipeIngredient) {
        if (updatedRecipeIngredient) {
            res.send({
                status: 200,
                message: "Recipe ingredient updated",
                id: updatedRecipeIngredient.id
            });
        } else {
            res.send({
                status: 500,
                message: "Cannot update recipe ingredient"
            });
        }
    });
});

// Add recipe step
router.post('/:recipe/steps', verifyToken, function (req, res) {
    var data = req.body;

    Recipe.findById(req.params.recipe).populate('user').exec().then(function (recipe) {
        if (recipe && req.user.id === recipe.user.id) {
            var recipeStep = new RecipeStep(data);

            recipeStep.save(function(err, createdRecipeStep) {
                if (createdRecipeStep) {
                    Recipe.update(
                        {_id: recipe.id},
                        {$push: {steps: createdRecipeStep}},
                        function (err) {
                            if (err) {
                                res.send({
                                    status: 500,
                                    message: "Cannot add step to recipe"
                                });
                            } else {
                                res.send({
                                    status: 200,
                                    message: "Recipe step added",
                                    id: createdRecipeStep.id
                                });
                            }
                        }
                    );
                } else {
                    res.send({
                        status: 500,
                        message: "Cannot create recipe step"
                    });
                }
            });
        } else {
            res.send({
                status: 500,
                message: "Cannot create recipe step"
            });
        }
    });
});

// Delete recipe step
router.delete('/:recipe/steps/:recipeStep', verifyToken, function (req, res) {
    Recipe.findOneAndUpdate(
        { _id: req.params.recipe },
        { $pull: { steps: req.params.recipeStep } },
        function (err, a) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot remove recipe step from recipe"
                });
            } else {
                RecipeStep.remove({ _id: req.params.recipeStep }, function(err) {
                    if (err) {
                        res.send({
                            status: 500,
                            message: "Cannot remove recipe step"
                        });
                    } else {
                        res.send({
                            status: 200,
                            message: "Recipe step removed"
                        });
                    }
                });
            }
        }
    );
});

// Update recipe step
router.put('/:recipe/steps/:recipeStep', verifyToken, function (req, res) {
    RecipeStep
    .findByIdAndUpdate(req.params.recipeStep, req.body)
    .then(function (updatedRecipeStep) {
        if (updatedRecipeStep) {
            res.send({
                status: 200,
                message: "Recipe step updated",
                id: updatedRecipeStep.id
            });
        } else {
            res.send({
                status: 500,
                message: "Cannot update recipe step"
            });
        }
    });
});

// Upload recipe step media
router.post('/:recipe/steps/:recipeStep/media', verifyToken, multipartMiddleware, function (req, res) {
    for (var i in req.files) {
        var file = req.files[i],
            newPath = fileService.getMediaFilePath(file);

        moveFile(file.path, process.cwd() + '/public' + newPath, function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot upload recipe step media"
                });
            } else {
                RecipeStep.findOneAndUpdate(
                    { _id: req.params.recipeStep },
                    { media: newPath },
                    function (err, a) {
                        if (err) {
                            res.send({
                                status: 500,
                                message: "Cannot upload recipe step media"
                            });
                        } else {
                            res.send({
                                status: 200,
                                message: "Recipe step media was uploaded",
                                path: newPath
                            });
                        }
                    }
                );
            }
        });
    }
});

// Upload recipe step thumbnail
router.post('/:recipe/steps/:recipeStep/thumbnail', verifyToken, multipartMiddleware, function (req, res) {
    for (var i in req.files) {
        var file = req.files[i],
            newPath = fileService.getThumbnailFilePath(file);

        moveFile(file.path, process.cwd() + '/public' + newPath, function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot upload recipe step thumbnail"
                });
            } else {
                RecipeStep.findOneAndUpdate(
                    { _id: req.params.recipeStep },
                    { thumbnail: newPath },
                    function (err, a) {
                        if (err) {
                            res.send({
                                status: 500,
                                message: "Cannot upload recipe step thumbnail"
                            });
                        } else {
                            res.send({
                                status: 200,
                                message: "Recipe step thumbnail was uploaded",
                                path: newPath
                            });
                        }
                    }
                );
            }
        });
    }
});

// Upload recipe image
router.post('/:recipe/image', verifyToken, multipartMiddleware, function (req, res) {
    for (var i in req.files) {
        var file = req.files[i],
            newPath = fileService.getMediaFilePath(file);

        moveFile(file.path, process.cwd() + '/public' + newPath, function (err) {
            if (err) {
                res.send({
                    status: 500,
                    message: "Cannot upload recipe image."
                });
            } else {
                Recipe.findOneAndUpdate(
                    { _id: req.params.recipe },
                    { image: newPath },
                    function (err, a) {
                        if (err) {
                            res.send({
                                status: 500,
                                message: "Cannot upload recipe image"
                            });
                        } else {
                            res.send({
                                status: 200,
                                message: "Recipe image was uploaded",
                                path: newPath
                            });
                        }
                    }
                );
            }
        });
    }
});

module.exports = router;
