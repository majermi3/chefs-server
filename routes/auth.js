'use strict';
var express = require('express');
var encrypt = require('../auth/encrypt');
var jwt = require('jsonwebtoken');
var auth = require('../auth/auth');
var User = require('../schemas/user');
var Token = require('../schemas/token');
var router = express.Router();


function removeOldTokens(user, callback) {
    Token.remove({ "user": user }, function (err) {
        callback(err);
    });
}

function createToken(user, callback) {
    removeOldTokens(user, function (err) {
        if (err) {
            callback(err);
        } else {
            var tokenString = jwt.sign(user, global.config.jwt_secret, {
                expiresIn: 1440
            });
            var refreshTokenString = jwt.sign({ token: tokenString }, global.config.jwt_secret, {
                expiresIn: 604800
            });
            var token = new Token({ token: tokenString, user: user });

            token.save(function (err) {
                callback(err, tokenString, refreshTokenString);
            });
        }
    });
}


/* Authenticate user */
router.post('/', function (req, res) {

    User.findOne({"email": req.body.email}).lean().exec(function (err, user) {
        if (err) {
            return res.send({ status: 500, message: "Something went wrong." });
        }
        if (!user) {
            return res.send({ status: 401, message: 'Wrong username or password.'});
        }
        var encryptedData = encrypt(req.body.password, user.salt);

        if (encryptedData.passwordHash === user.password) {
            createToken(user, function (err, token, refreshToken) {
                if (err) {
                    return res.send({ status: 500, message: "Something went wrong." });
                }
                return res.send({ status: 200, token: token, refreshToken: refreshToken });
            });
        } else {
            return res.send({ status: 401, message: "Wrong username or password."})
        }
    })
});

router.post('/oauth', function (req, res) {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    auth.verifyByGoogle(token, function (user) {
        return res.send({ status: 200 });
    }, function (err) {
        return res.send({ status: 401 });
    })
});

router.post('/token', function (req, res) {

    if (req.body.refreshToken) {
        // Verify refresh token
        jwt.verify(req.body.refreshToken, global.config.jwt_secret, function (err, t) {
            if (err) {
                return res.send({ status: 401, message: 'Access denied.' });
            }
            // Find user by token
            Token.findOne({ token: t.token }).populate('user').lean().exec(function (err, tokenObj) {

                if (err) {
                    return res.send({ status: 500, message: "Something went wrong." });
                }
                if (!tokenObj) {
                    return res.send({ status: 400, message: 'Bad request' });
                }

                // Create new tokens
                createToken(tokenObj.user, function (err, token, refreshToken) {
                    if (err) {
                        return res.send({ status: 500, message: "Something went wrong." });
                    }
                    return res.send({ status: 200, token: token, refreshToken: refreshToken });
                });
            });
        });
    } else {
        return res.send({ status: 400, message: 'Bad request' });
    }
});

module.exports = router;
