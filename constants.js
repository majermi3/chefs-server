'use strict';
module.exports = Object.freeze({
    ACCESS_LEVEL: {
        PRIVATE: 1,
        FRIEND: 2,
        FOF: 3,
        PUBLIC: 4
    }
});
