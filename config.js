var config = {
    development: {
        //mongodb connection settings
        database: {
            host: '127.0.0.1',
            port: '27017',
            db: 'chef'
        },
        //server details
        server: {
            host: '127.0.0.1',
            port: '3000'
        },
        email: {
            user: 'majernik.chef@gmail.com',
            pass: 'Cd+Bq9E#BGR'
        },
        baseUrl: 'http://majernik-chefs-server.my-gateway.de',
        jwt_secret: "14skrhm47eg5cdft98",
        client_id: "379979095337-cgg9gih5l6mv1pa3do94rj2julh2bspt.apps.googleusercontent.com",
        fcm_api_key: "AAAAWHiE3Sk:APA91bE22v8tJADs1kY641AHYEW9zWgQVfA8m2Ma1uio2n2_NQQuk4-msyUTR7uxZ8Xp8faIeKL47Le6YtvdNa-_KwNj-W4h-Ybw9x6dk5JHhcRqdupjGpA3LhmbMa8OItg0XtfRf_L9"
    },
    production: {
        //mongodb connection settings
        database: {
            host: '127.0.0.1',
            port: '27017',
            db: 'chef'
        },
        //server details
        server: {
            host: '127.0.0.1',
            port: '3000'
        },
        jwt_secret: "14skrhm47eg5cdft98",
        client_id: "379979095337-cgg9gih5l6mv1pa3do94rj2julh2bspt.apps.googleusercontent.com",
        fcm_api_key: "AAAAWHiE3Sk:APA91bE22v8tJADs1kY641AHYEW9zWgQVfA8m2Ma1uio2n2_NQQuk4-msyUTR7uxZ8Xp8faIeKL47Le6YtvdNa-_KwNj-W4h-Ybw9x6dk5JHhcRqdupjGpA3LhmbMa8OItg0XtfRf_L9"
    }
};
config.production.email = config.development.email
config.production.baseUrl = config.development.baseUrl

module.exports = config;
