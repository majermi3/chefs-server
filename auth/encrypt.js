'use strict';
var crypto = require('crypto');

var genRandomString = function (length) {
	return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')
        .slice(0, length);
};
module.exports = function (password, salt) {
	salt = salt || genRandomString(16);

	var hash = crypto.createHmac('sha512', salt);
	hash.update(password);
	var value = hash.digest('hex');

	return {
		salt: salt,
		passwordHash: value
	};
};