var User = require('../schemas/user');
var auth = require('./auth');

module.exports = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
        auth.verifyByGoogle(token, function(user) {
            req.user = user;
            next();
        }, function(err) {
            return res.status(403).send({ status: 403, message: "Access denied."});
        }).catch(function(err) {
            console.error(err);

            auth.verifyJwtToken(token, function (user) {
                req.user = user;
                next();
            }, function (err) {
                return res.status(403).send({ status: 403, message: "Access denied."});
            });
        });
    } else {
        // forbidden without token
        return res.status(403).send({ status: 403, message: "Access denied." });
    }
};
