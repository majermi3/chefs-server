var jwt = require('jsonwebtoken');
var User = require('../schemas/user');

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(global.config.client_id);

var auth = {};

auth.verifyByGoogle = async function (token, callback, err) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: global.config.client_id
  });
  const payload = ticket.getPayload();
  const userId = payload['sub'];

  User.findOne({
      provider: 'google',
      providerId: userId
  }).exec(function (errObj, userObj) {
      if (errObj) {
          err(errObj);
      } else {
          if (userObj) {
              callback(userObj);
          } else {
              err(errObj);
          }
      }
  })
}

auth.verifyJwtToken = function (token, callback, err) {
    jwt.verify(token, global.config.jwt_secret, function (errObj, user) {
        if (errObj) { //failed verification.
            err(errObj);
        }
        User.findById(user._id).exec(function (errObj, user) {
            if (errObj) {
                err(errObj);
            } else {
                callback(user);
            }
        })
    });
}

module.exports = auth;
