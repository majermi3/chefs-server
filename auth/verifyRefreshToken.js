var crypto = require('crypto')

function rotate(text) {
	var shiftBy = Math.ceil(text.length / 3);
	for (var i = 0; i < shiftBy; i++) {
		text = text[text.length - 1] + text.substring(0, text.length - 1);
	}
	return text;
}

function generateRefreshToken(email) {
	return crypto
		.createHash('sha512')
		.update(rotate(email), 'utf8')
		.digest('hex');
}

module.exports = function (refreshToken, email) {
	var token = generateRefreshToken(email);;
	return refreshToken === token;
};