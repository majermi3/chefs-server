var request = require('request');
var notificationService = {};

function sendMessage(data, deviceId, err, callback) {
    request({
        url: 'https://fcm.googleapis.com/fcm/send',
        method: 'POST',
        headers: {
          'Content-Type' :' application/json',
          'Authorization': 'key=' + global.config.fcm_api_key
        },
        body: JSON.stringify({
            "data": data,
            "to" : deviceId
        })
    },
    function(error, response, body) {
        if (error) {
            if (typeof err === "function") {
                err(error);
            }
        } else if (response.statusCode >= 400) {
            if (typeof err === "function") {
                err(response.statusMessage);
            }
        } else {
            if (typeof callback === "function") {
                callback();
            }
        }
  });
}

notificationService.sendFriendshipRequestNotification = function (currentUser, friend, err, callback) {
    var data = {
        "message": currentUser.name + " would like to becomme your friend",
        "user": {
            id: currentUser._id,
            name: currentUser.name,
            email: currentUser.email
        },
        "type": "new_user"
    };
    sendMessage(data, friend.fcmRefreshToken, err, callback);
};
notificationService.sendAcceptFriendshipNotification = function (currentUser, friend, err, callback) {
    var data = {
        "message": currentUser.name + " accepted your friendship request",
        "user": {
            id: currentUser._id,
        },
        "type": "request_accepted"
    };
    sendMessage(data, friend.fcmRefreshToken, err, callback);
}


module.exports = notificationService;
