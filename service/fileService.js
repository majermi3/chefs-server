var fileService = {};

fileService.isImage = function(fileName) {
    return /(\.jpg|\.jpeg|\.gif|\.png|\.bmp)$/.test(fileName);
}

fileService.getMediaFolder = function(origFilename) {
    return origFilename
        .substr(0, origFilename.indexOf("."))
        .replace(/(.{4})/g, "$1/");
}

fileService.getThumbnailFilePath = function(file) {
    var origFilename = file.originalFilename;
    return '/thumbnails/' + this.getMediaFolder(origFilename) + origFilename;
}

fileService.getMediaFilePath = function(file) {
    var origFilename = file.originalFilename;
    var directory = this.getMediaFolder(origFilename);
    if (this.isImage(origFilename)) {
        return '/images/' + directory + origFilename;
    }
    return '/videos/' + directory + origFilename;
}

module.exports = fileService;
