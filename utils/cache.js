
var mcache = require('memory-cache');

var cache = (duration) => {
	return (req, res, next) => {
		let key = '__chef__' + req.originalUrl || req.url
		let cacheBody = mcache.get(key)
		if (cacheBody) {
			res.send(cacheBody)
		} else {
			res.sendResponse = res.send
			res.send = (body) => {
				mcache.put(key, body, duration * 1000)
				res.sendResponse(body)
			}
			next()
		}
	}
}

module.exports = cache;
