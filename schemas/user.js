var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    "created": {
        type: Date,
        default: Date.now
    },
    "updated": {
        type: Date,
        default: Date.now
    },
    "name": {
        type: String
    },
    "email": {
        type: String
    },
    "image": {
        type: String
    },
    "password": {
        type: String
    },
    "provider": {
        type: String,
        default: 'chefs_app'
    },
    "providerId": {
        type: String,
    },
    "externalId": {
        type: String
    },
    "salt": {
        type: String
    },
    "fcmRefreshToken": {
        type: String
    },
    "favoriteRecipes": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Recipe'
    }],
    "friends": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    "pendingRequests": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
});

module.exports = mongoose.model('User', userSchema, 'user');
