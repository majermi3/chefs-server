var mongoose = require('mongoose');
var constants = require('../constants');

var recipeSchema = mongoose.Schema({
    "title": {
        type: String
    },
    "description": {
        type: String
    },
    "image": {
        type: String
    },
    "user": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    "ingredients": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RecipeIngredient'
    }],
    "steps": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RecipeStep'
    }],
    "accessLevel": {
        type: Number,
        enum: Object.values(constants.ACCESS_LEVEL),
        default: constants.ACCESS_LEVEL.PRIVATE
    },
    "deleted": {
        type: Boolean,
        default: false
    },
});

module.exports = mongoose.model('Recipe', recipeSchema, 'recipe');
