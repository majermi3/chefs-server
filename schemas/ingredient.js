var mongoose = require('mongoose');

var ingredientSchema = mongoose.Schema({
    "title": {
        type: String
    },
    "liquid": {
        type: Boolean,
        default: false
    },
    "verified": {
        type: Boolean,
        default: false
    },
    "user": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    "type": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'IngredientType'
    }
});

module.exports = mongoose.model('Ingredient', ingredientSchema, 'ingredient');
