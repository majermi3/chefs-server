var mongoose = require('mongoose');

var recipeIngredientSchema = mongoose.Schema({
	"ingredient": {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Ingredient'
	},
	"unit": {
		type: Number
	},
	"amount": {
		type: Number
	},
	"metricUnitSystem": {
        type: Boolean,
        default: true
	}
});

module.exports = mongoose.model('RecipeIngredient', recipeIngredientSchema, 'recipeIngredient');
