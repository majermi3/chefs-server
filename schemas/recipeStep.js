var mongoose = require('mongoose');

var recipeStepSchema = mongoose.Schema({
	"description": {
		type: String
	},
    "position": {
        type: Number
    },
    "media": {
        type: String
    },
	"thumbnail": {
		type: String
	}
});

module.exports = mongoose.model('RecipeStep', recipeStepSchema, 'recipeStep');
