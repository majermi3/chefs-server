var mongoose = require('mongoose');

var tokenSchema = mongoose.Schema({
    "token": {
        type: String
    },
    "user": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Token', tokenSchema, 'token');