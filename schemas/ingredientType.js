var mongoose = require('mongoose');

var ingredientTypeSchema = mongoose.Schema({
    "title": {
        type: String
    },
    "parent": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'IngredientType'
    }
});

module.exports = mongoose.model('IngredientType', ingredientTypeSchema, 'ingredientType');